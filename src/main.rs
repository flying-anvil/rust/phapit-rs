use phapit::*;

fn main() {
    println!("Hello, world!");

    println!("bindec(\"10011011\"): {}", bindec("-10011011").unwrap());

    println!("{}", format!("{:b}", 155));
    println!("{}", format!("{}", (-14).decbin()));
}
