use std::num::ParseIntError;

/// Converts a binary string (`"1100101"`) into an integer.
///
/// It also assumes the input to represent an unsigned value. However, the input can have a leading `+` or `-`
/// to indicate the sign (which still treats it as unsigned, as in no two's complement).
///
/// # Examples
/// ```
/// use phapit::*;
///
/// assert_eq!(bindec("10011011"), Ok(155));
/// assert_eq!(bindec("+10011011"), Ok(155));
/// assert_eq!(bindec("-10011011"), Ok(-155));
/// ```
///
/// # Difference to PHP:
/// - Does not try to recover from an invalid binary string (like `"1123"`). Instead, it results in a ParseIntError of king InvalidDigit.
/// - Does not fallback to a float if the value is too big for an integer. Instead, it results in a ParseIntError of king PosOverflow/NegOverflow.
/// - Returns a Result<isize, ParseIntError> instead of a number (because it's Rust and the conversion might fail).
pub fn bindec(binary_string: &str) -> Result<isize, ParseIntError> {
    isize::from_str_radix(binary_string, 2)
}

#[cfg(test)]
mod tests {
    use std::num::IntErrorKind;

    use crate::math::bindec;

    #[test]
    fn implicit_positive() {
        assert_eq!(bindec("10011011"), Ok(155));
    }

    #[test]
    fn explicit_positive() {
        assert_eq!(bindec("+10011011"), Ok(155));
    }

    #[test]
    fn explicit_negative() {
        assert_eq!(bindec("-10011011"), Ok(-155));
    }

    #[test]
    fn error_invalid_digit() {
        let result = bindec("1123");

        assert!(result.is_err());
        assert_eq!(result.unwrap_err().kind(), &IntErrorKind::InvalidDigit);
    }

    #[test]
    fn error_pos_overflow() {
        let result = bindec("1".repeat(64).as_str());

        assert!(result.is_err());
        assert_eq!(result.unwrap_err().kind(), &IntErrorKind::PosOverflow);
    }

    #[test]
    fn error_neg_overflow() {
        let result = bindec(format!("-{}", "1".repeat(64)).as_str());

        assert!(result.is_err());
        assert_eq!(result.unwrap_err().kind(), &IntErrorKind::NegOverflow);
    }
}
