use std::fmt::Binary;

#[deprecated(note="Use the decbin method on a number instead: my_number.decbin()")]
pub fn decbin() {}

pub trait DecBin {
    fn decbin(&self) -> String;
}

impl<T: Binary> DecBin for T {
    fn decbin(&self) -> String {
        format!("{:b}", self)
    }
}

#[cfg(test)]
mod tests {
    use crate::math::DecBin;

    #[test]
    fn test() {
        let number = 155;

        assert_eq!(number.decbin(), "10011011");
        assert_eq!((-number).decbin(), "11111111111111111111111101100101");
    }
}
