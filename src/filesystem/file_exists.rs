use std::path::Path;

/// Checks if a given path exists as a file or directory.
///
/// # Examples
/// ```
/// use phapit::*;
///
/// assert_eq!(file_exists("./src/lib.rs"), true);
/// assert_eq!(file_exists("/tmp/non-existent"), false);
/// ```
pub fn file_exists(path: impl AsRef<Path>) -> bool {
    path.as_ref().exists()
}
