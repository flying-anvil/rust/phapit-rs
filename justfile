lint:
  cargo check
  cargo clippy

book +CMD:
  cd book && mdbook {{CMD}}
