# Math

All trigonometry function that exist PHP are also present in Rust.
However, they are not a standalone functions that gets passed a number, but a member function of each of the floating number types.
Instead of `atan2($number)`, use `number.atan2()`.

---

List of math functions:

- … means _todo_
- ✅ means yes
- ❌ means decided not to include it
- ❓ means decided to be unsure

|                         Name | PHP‑Doc                                                           | Description (from PHP)                                  | Covered          |          Part of the crate (…/✅/❌/❓) |
|:-----------------------------|-------------------------------------------------------------------|---------------------------------------------------------|------------------|-----------------------------------------|
| [abs](./abs.md)                   | [Link](https://www.php.net/manual/en/function.abs.php)         | Absolute value                                          | ✅               | ✅* As hint with `#[deprecated]`        |
| [acos](./acos.md)                 | [Link](https://www.php.net/manual/en/function.acos.php)        | Arc cosine                                              | ✅               | ✅* As hint with `#[deprecated]`        |
| [acosh](./acosh.md)               | [Link](https://www.php.net/manual/en/function.acosh.php)       | Inverse hyperbolic cosine                               | ✅               | ✅* As hint with `#[deprecated]`        |
| [asin](./asin.md)                 | [Link](https://www.php.net/manual/en/function.asin.php)        | Arc sine                                                | …                | …                                       |
| [asinh](./asinh.md)               | [Link](https://www.php.net/manual/en/function.asinh.php)       | Inverse hyperbolic sine                                 | …                | …                                       |
| [atan2](./atan2.md)               | [Link](https://www.php.net/manual/en/function.atan2.php)       | Arc tangent of two variables                            | …                | …                                       |
| [atan](./atan.md)                 | [Link](https://www.php.net/manual/en/function.atan.php)        | Arc tangent                                             | …                | …                                       |
| [atanh](./atanh.md)               | [Link](https://www.php.net/manual/en/function.atanh.php)       | Inverse hyperbolic tangent                              | …                | …                                       |
| [base_convert](./base_convert.md) | [Link](https://www.php.net/manual/en/function.base-convert.php)| Convert a number between arbitrary bases                | …                | …                                       |
| [bindec](./bindec.md)             | [Link](https://www.php.net/manual/en/function.bindec.php)      | Binary to decimal                                       | ✅               | ✅ Uses `isize` as the type             |
| [ceil](./ceil.md)                 | [Link](https://www.php.net/manual/en/function.ceil.php)        | Round fractions up                                      | …                | …                                       |
| [cos](./cos.md)                   | [Link](https://www.php.net/manual/en/function.cos.php)         | Cosine                                                  | …                | …                                       |
| [cosh](./cosh.md)                 | [Link](https://www.php.net/manual/en/function.cosh.php)        | Hyperbolic cosine                                       | …                | …                                       |
| [decbin](./decbin.md)             | [Link](https://www.php.net/manual/en/function.decbin.php)      | Decimal to binary                                       | ✅               | ✅* As hint with `#[deprecated]` / as a member function on any type implementing `std::fmt::Binary` |
| [dechex](./dechex.md)             | [Link](https://www.php.net/manual/en/function.dechex.php)      | Decimal to hexadecimal                                  | …                | …                                       |
| [decoct](./decoct.md)             | [Link](https://www.php.net/manual/en/function.decoct.php)      | Decimal to octal                                        | …                | …                                       |
| [deg2rad](./deg2rad.md)           | [Link](https://www.php.net/manual/en/function.deg2rad.php)     | Converts the number in degrees to the radian equivalent | …                | …                                       |
| [exp](./exp.md)                   | [Link](https://www.php.net/manual/en/function.exp.php)         | Calculates the exponent of *e*                          | …                | …                                       |
| [expm1](./expm1.md)               | [Link](https://www.php.net/manual/en/function.expm1.php)       | Returns exp($num) - 1, computed in a way that is accurate even when the value of number is close to zero | …         | …                    |
| [fdiv](./fdiv.md)                 | [Link](https://www.php.net/manual/en/function.fdiv.php)        | Divides two numbers, according to IEEE 754              | …                | …                                       |
| [floor](./floor.md)               | [Link](https://www.php.net/manual/en/function.floor.php)       | Round fractions down                                    | …                | …                                       |
| [fmod](./fmod.md)                 | [Link](https://www.php.net/manual/en/function.fmod.php)        | Returns the floating point remainder (modulo) of the division of the arguments | …                                   | …                    |
| [hexdec](./hexdec.md)             | [Link](https://www.php.net/manual/en/function.hexdec.php)      | Hexadecimal to decimal                                  | …                | …                                       |
| [hypot](./hypot.md)               | [Link](https://www.php.net/manual/en/function.hypot.php)       | Calculate the length of the of a right-angle triangle   | …                | …                                       |
| [intdiv](./intdiv.md)             | [Link](https://www.php.net/manual/en/function.intdiv.php)      | Integer division                                        | …                | …                                       |
| [is_finite](./is_finite.md)       | [Link](https://www.php.net/manual/en/function.is-finite.php)   | Finds whether a value is a legal finite number          | …                | …                                       |
| [is_infinite](./is_infinite.md)   | [Link](https://www.php.net/manual/en/function.is-infinite.php) | Finds whether a value is infinite                       | …                | …                                       |
| [is_nan](./is_nan.md)             | [Link](https://www.php.net/manual/en/function.is-nan.php)      | Finds whether a value is not a number                   | …                | …                                       |
| [log10](./log10.md)               | [Link](https://www.php.net/manual/en/function.log10.php)       | Base-10 logarithm                                       | …                | …                                       |
| [log1p](./log1p.md)               | [Link](https://www.php.net/manual/en/function.log1p.php)       | Returns log(1 + number), computed in a way that is accurate even when the value of number is close to zero | …       | …                    |
| [log](./log.md)                   | [Link](https://www.php.net/manual/en/function.log.php)         | Natural logarithm                                       | …                | …                                       |
| [max](./max.md)                   | [Link](https://www.php.net/manual/en/function.max.php)         | Find highest value                                      | …                | …                                       |
| [min](./min.md)                   | [Link](https://www.php.net/manual/en/function.min.php)         | Find lowest value                                       | …                | …                                       |
| [octdec](./octdec.md)             | [Link](https://www.php.net/manual/en/function.octdec.php)      | Octal to decimal                                        | …                | …                                       |
| [pi](./pi.md)                     | [Link](https://www.php.net/manual/en/function.pi.php)          | Get value of pi                                         | …                | …                                       |
| [pow](./pow.md)                   | [Link](https://www.php.net/manual/en/function.pow.php)         | Exponential expression                                  | …                | …                                       |
| [rad2deg](./rad2deg.md)           | [Link](https://www.php.net/manual/en/function.rad2deg.php)     | Converts the radian number to the equivalent number in degrees | …         | …                                       |
| [round](./round.md)               | [Link](https://www.php.net/manual/en/function.round.php)       | Rounds a float                                          | …                | …                                       |
| [sin](./sin.md)                   | [Link](https://www.php.net/manual/en/function.sin.php)         | Sine                                                    | …                | …                                       |
| [sinh](./sinh.md)                 | [Link](https://www.php.net/manual/en/function.sinh.php)        | Hyperbolic sine                                         | …                | …                                       |
| [sqrt](./sqrt.md)                 | [Link](https://www.php.net/manual/en/function.sqrt.php)        | Square root                                             | …                | …                                       |
| [tan](./tan.md)                   | [Link](https://www.php.net/manual/en/function.tan.php)         | Tangent                                                 | …                | …                                       |
| [tanh](./tanh.md)                 | [Link](https://www.php.net/manual/en/function.tanh.php)        | Hyperbolic tangent                                      | …                | …                                       |
|                              |                                                         | …                | …                                       |
