# `decbin`

## PHP

It's a function that accepts an integer and its binary representation.
The result is not aligned to a nice byte-bound (`1110` instead of `00001110`).

### Signature

`decbin(int $num): string`

### Examples

```php
var_dump(decbin(155) === '10011011');
```

## Rust

In Rust, `bindec` does not exist standalone functions.
Instead, integers can be formatted into strings in various formats, including binary.

That is done by using the binary (`b`) formatter in a function/marco like `format!()`.

Transitionally, you can `use phapit::DecBin` and use `my_number.decbin()`.
Technically, this method is available on every type that can be represented in binary,

### Examples

```rust
assert_eq!(format!("{:b}", 155u8), "10011011");
assert_eq!(format!("{:b}", 155u64), "10011011");

assert_eq!(format!("{:b}", -155), "11111111111111111111111101100101");

/*
use phapit::*;

let number = 155;

assert_eq!(number.decbin(), "10011011");
assert_eq!((-number).decbin(), "11111111111111111111111101100101");
*/
```
