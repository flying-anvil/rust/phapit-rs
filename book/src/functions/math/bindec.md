# `bindec`

## PHP

It's a function that accepts a string representing bits (`"10011011"`) and returns the parsed number.
All values are interpreted as unsigned numbers.

An invalid binary string (`"1123"`) will be interpreted as `"11"`, result ing in a `3`.

### Signature

`bindec(string $binary_string): int|float`

### Examples

```php
var_dump(bindec('10011011') === 155);
```

## Rust

In Rust, `bindec` does not exist standalone functions.
Instead, integers can be parsed from strings of all kinds of bases/radixes.

Because the conversion can fail (when the number does not fit into the type, invalid digits (like a `"2"`)),
it returns a `Result<Self, ParseIntError>`.

It also assumes the input to represent an unsigned value. However, the input can have a leading `+` or `-`
to indicate the sign (which still treats it as unsigned, as in no two's complement).

Transitionally, you can use `phapit::bindec("10011011")`.

### Examples

```rust
assert_eq!(u8::from_str_radix("10011011", 2), Ok(155));
assert_eq!(u32::from_str_radix("+10011011", 2), Ok(155));
assert_eq!(i32::from_str_radix("-10011011", 2), Ok(-155));

/*
use phapit::*;
assert_eq!(decbin("10011011"), Ok(155));
assert_eq!(decbin("+10011011"), Ok(155));
assert_eq!(decbin("-10011011"), Ok(-155));
*/
```
