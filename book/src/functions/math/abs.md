# `abs`

Returns the absolute value of a number.

## PHP

It's a function that accepts a number (int or float) and returns its absolute value.

### Signature

`abs(int|float $num): int|float`

### Examples

```php
var_dump(abs(-4.2) === 4.2);
var_dump(abs(5) === 5);
var_dump(abs(-5) === 5);
```

## Rust

In Rust, `abs` is not a standalone functions that can accept any number.
Instead, it's a method on each of the number types that can be negative
(`f32`, `f64`, `i8`, `i16`, `i32`, `i64`, `i128`, `isize`).
The unsigned number types don't have an `abs` function because they cannot be negative in the first place.

### Examples

```rust
let negative = -4.2f32;
let positive = 5i16;

assert_eq!(negative.abs(), 4.2);
assert_eq!(positive.abs(), 5);
```
