# `acosh`

Returns the inverse hyperbolic cosine of a number.

## PHP

It's a function that accepts a number (float) and returns its inverse hyperbolic cosine.

### Signature

`acosh(float $num): float`

### Examples

```php
var_dump(acosh(2.0));
var_dump(acosh(1.0));
var_dump(acosh(0.0));
var_dump(acosh(-1.0));
```

## Rust

In Rust, `acosh` is not a standalone functions that can accept any number.
Instead, it's a method on each of the floating number types.

### Examples

```rust
let number: f64 = 2.0;
assert_eq!(number.acosh(), 1.3169578969248166);

let number: f64 = 1.0;
assert_eq!(number.acosh(), 0.0);

let number: f64 = 0.0;
assert!(number.acosh().is_nan());

let number: f64 = -1.0;
assert!(number.acosh().is_nan());
```
