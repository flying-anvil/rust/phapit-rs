# `acos`

Returns the arc cosine of a number.

## PHP

It's a function that accepts a number (float) and returns its arc cosine (in radians).

### Signature

`acos(float $num): float`

### Examples

```php
var_dump(acos(.3));
var_dump(acos(1.1));
```

## Rust

In Rust, `acos` is not a standalone functions that can accept any number.
Instead, it's a method on each of the floating number types.

It's in radians too.

### Examples

```rust
let number: f64 = 0.3;
assert_eq!(number.acos(), 1.2661036727794992);

let number: f64 = 1.1;
assert!(number.acos().is_nan());
```
