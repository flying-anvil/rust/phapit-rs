# `file_exists`

Returns whether a given path exists (file or directory).

## PHP

It's a function that accepts a string-path and returns a bool.

### Signature

`file_exists(string $filename): bool`

### Examples

```php
var_dump(file_exists('/etc/passwd'));
```

## Rust

In Rust, `file_exists` is not a standalone functions.
Instead, a `std::path::Path` can be used to check for existence.

### Examples

```rust
use std::path::Path;

let path = Path::new("./src/lib.rs");

assert_eq!(path.exists(), true);
```
