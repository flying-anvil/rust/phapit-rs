# `TODO`

Returns _TODO_.

## PHP

It's a function that accepts a _TODO_ and returns _TODO_.

### Signature

`TODO(TODO): TODO`

### Examples

```php
var_dump(TODO === TODO);
```

## Rust

In Rust, `TODO` is not a standalone functions.
Instead, it's a method on _TODO_ types.

### Examples

```rust
let number = TODO;

assert_eq!(number.TODO(), TODO);
```
