# Most Used

A "curated" list of functions that are used commonly.

---

List of filesystem functions:

- … means _todo_
- ✅ means yes
- ❌ means decided not to include it
- ❓ means decided to be unsure

|                         Name                    | PHP‑Doc                                                                | Description (from PHP)                                      | Covered          |          Part of the crate (…/✅/❌/❓) |
|:------------------------------------------------|------------------------------------------------------------------------|-------------------------------------------------------------|------------------|-----------------------------------------|
| **Files** |
| [file_exists](../filesystem/file_exists.md)     | [Link](https://www.php.net/manual/en/function.file-exists.php)         | Checks whether a file or directory exists                   | ✅               | ✅                                      |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
| **Math**                                        |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
|                                                 |                                                                        |                                                             |                  |                                         |
