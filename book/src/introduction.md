# Introduction

Phapit-RS is a library and a book intended to ease migration from PHP to Rust.

The library contains Rust functions (and potentially "classes") that mimic PHP functionality.
The intend is not to use them 1-to-1, but to have a resource to look into on how to do PHP things in Rust.

This book is a written guide that's easier to read.
